output "DigitalPool" {
  value = "${aws_cognito_user_pool.DigitalPool.id}"
}

output "API base_url" {
  value = "${aws_api_gateway_deployment.restapi.invoke_url}"
}