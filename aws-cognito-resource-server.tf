resource "aws_cognito_resource_server" "DigitalPool" {
  identifier = "DigitalClient"
  name       = "DigitalClient"

  scope {
    scope_name        = "DigitalClientScope"
    scope_description = "DigitalClientScope"
  }

  user_pool_id = "${aws_cognito_user_pool.DigitalPool.id}"
}