resource "aws_lambda_function" "mylambda" {
   function_name = "myLambda"

   # The bucket name as created earlier with "aws s3api create-bucket"
   s3_bucket = "terraform-serverless-mylambda"
   s3_key    = "myLambda.zip"

   # "main" is the filename within the zip file (main.js) and "handler"
   # is the name of the property under which the handler function was
   # exported in that file.
   handler = "Auth::Auth.Function::FunctionHandler"
   runtime = "dotnetcore3.1"

   role = "${aws_iam_role.lambda_exec.arn}"
}

 # IAM role which dictates what other AWS services the Lambda function
 # may access.
resource "aws_iam_role" "lambda_exec" {
   name = "my_lambda"

   assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_lambda_permission" "mylambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.mylambda.function_name}"
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.restapi.execution_arn}/*/*/*"
}