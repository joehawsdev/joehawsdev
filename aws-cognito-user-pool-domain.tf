resource "aws_cognito_user_pool_domain" "domain_pool" {
  domain = "digitalteam"
  user_pool_id = "${aws_cognito_user_pool.DigitalPool.id}"
}