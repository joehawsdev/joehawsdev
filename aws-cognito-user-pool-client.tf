resource "aws_cognito_user_pool_client" "DigitalPool" {
  name = "DigitalClient"

  user_pool_id = "${aws_cognito_user_pool.DigitalPool.id}"

  generate_secret                      = true
  explicit_auth_flows                  = ["ALLOW_REFRESH_TOKEN_AUTH"]
  allowed_oauth_flows                  = ["client_credentials"]
  allowed_oauth_flows_user_pool_client = true

  allowed_oauth_scopes = [
    "DigitalClient/DigitalClientScope",
  ]

  refresh_token_validity = 1
}