using Amazon.Lambda.Core;
using Auth.Models;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using static System.Environment;


namespace Auth.Helper
{
    public class HttpHelper
    {
		public object Result { get; internal set; }

		public async Task<Token> GetToken(ILambdaContext context)

        {
            var clientId = "54q4p9qebkr2calt7vr5gc0hak";
            var clientSecret = "rjlq2oi3814o2k5blhnd2luovs9k28mnpc9mbal2qt70ia41ddg";
            var url = "https://digitalteam.auth.eu-west-1.amazoncognito.com/oauth2/token";

            context.Logger.LogLine($"INFO ClientId: {clientId}, ClientSecret: {clientSecret}");
            context.Logger.LogLine($"INFO Obtaining JWT: {clientId}, {clientSecret}, {url}");

            var body = $"grant_type=client_credentials&client_id={clientId}";
            var base64Authorization = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{clientId}:{clientSecret}"));

			using var httpClient = new HttpClient();
			using var request = new HttpRequestMessage(new HttpMethod("POST"), url);
			request.Headers.TryAddWithoutValidation("content-type", "application/x-www-form-urlencoded");
			request.Headers.TryAddWithoutValidation("Authorization", $"Basic {base64Authorization}");
			request.Content = new StringContent(body, Encoding.UTF8, "application/x-www-form-urlencoded");//CONTENT-TYPE header

			var response = await httpClient.SendAsync(request);
			var jwt = await response.Content.ReadAsStringAsync();

			context.Logger.LogLine($"INFO Retrieved JWT: {jwt}");

			var token = JsonConvert.DeserializeObject<Token>(jwt);

			return token;
		}
    }
}