using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using System;
using Auth.Helper;
using Auth.Models;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace AwsTokenGet
{
	public class Function
	{
		public readonly HttpHelper _HttpHelper = new HttpHelper();
		public readonly APIGatewayProxyResponse success = new APIGatewayProxyResponse
		
		{
			StatusCode = 200,
			Body = ""
		};

		public Function()
		{


		}
		public void FunctionHandler(ILambdaContext context, HttpHelper GetToken)
		{
			try
			{
				var token = GetToken.Result;
				Console.Out.Write(token);
				context.Logger.LogLine($"ERROR {token}");
				Console.WriteLine("Hello World!");

			}
			catch (Exception e)
			{
				context.Logger.LogLine($"ERROR {e.Message} {e.StackTrace}");
			}
		}
	}
}