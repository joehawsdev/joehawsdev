resource "aws_api_gateway_rest_api" "restapi" {
	name			= "ApiRestAPI"
	description		= "ApiRestAPI for Digital Team"
}

resource "aws_api_gateway_resource" "proxy" {
	rest_api_id		= "${aws_api_gateway_rest_api.restapi.id}"
	parent_id		= "${aws_api_gateway_rest_api.restapi.root_resource_id}"
	path_part		= "{proxy+}"
}

resource "aws_api_gateway_method" "proxy" {
	rest_api_id		= "${aws_api_gateway_rest_api.restapi.id}"
	resource_id		= "${aws_api_gateway_resource.proxy.id}"
	http_method		= "ANY"
	authorization	= "NONE"
}

resource "aws_api_gateway_integration" "lambda" {
	rest_api_id		= "${aws_api_gateway_rest_api.restapi.id}"
	resource_id		= "${aws_api_gateway_method.proxy.resource_id}"
	http_method		= "${aws_api_gateway_method.proxy.http_method}"

	integration_http_method = "POST"
	type					= "AWS_PROXY"
	uri						= "${aws_lambda_function.mylambda.invoke_arn}"
}

resource "aws_api_gateway_method" "proxy_root" {
	rest_api_id		= "${aws_api_gateway_rest_api.restapi.id}"
	resource_id		= "${aws_api_gateway_rest_api.restapi.root_resource_id}"
	http_method		= "ANY"
	authorization	= "NONE"
}

resource "aws_api_gateway_integration" "lambda_root" {
   rest_api_id = "${aws_api_gateway_rest_api.restapi.id}"
   resource_id = "${aws_api_gateway_method.proxy_root.resource_id}"
   http_method = "${aws_api_gateway_method.proxy_root.http_method}"

   integration_http_method = "POST"
   type                    = "AWS_PROXY"
   uri                     = "${aws_lambda_function.mylambda.invoke_arn}"
}

resource "aws_api_gateway_deployment" "restapi" {
   depends_on = [
     "aws_api_gateway_integration.lambda",
     "aws_api_gateway_integration.lambda_root"
   ]

   rest_api_id = "${aws_api_gateway_rest_api.restapi.id}"
   stage_name  = "digitalteam"
}

